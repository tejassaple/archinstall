#!/bin/bash

ln -sf /usr/share/zoneinfo/Asia/Kolkata /etc/localtime
hwclock --localtime --adjust
#sed -i '178s/.//' /etc/locale.gen
sed -i "/#en_US.UTF-8 UTF-8/s/^#//g" /etc/locale.gen
locale-gen
echo "aloy" >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 aloy.localdomain aloy" >> /etc/hosts
echo root:password | chpasswd

pacman -S vim grub grub-btrfs efibootmgr dosfstools os-prober mtools networkmanager btrfs-progs ntfs-3g base-devel git bluez openssh sudo

grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB --recheck
grub-mkconfig -o /boot/grub/grub.cfg

#pacman -S mesa xorg sddm plasma konsole kate dolphin firefox
pacman -S nvidia-dkms nvidia-utils lib32-nvidia-utils nvidia-settings vulkan-icd-loader lib32-vulkan-icd-loader xorg sddm plasma konsole kate dolphin firefox

systemctl enable NetworkManager
systemctl enable sddm

useradd -m tejas
echo tejas:password | chpasswd
usermod -aG wheel tejas

echo "tejas ALL=(ALL) ALL" >> /etc/sudoers.d/tejas
